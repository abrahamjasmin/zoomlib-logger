## Description

ZOOM Logger implement PSR-3 logger

## Installation

Install with [Composer](http://getcomposer.org):

    composer require zoom/zoomlib-logger

## Usage 

```php
use Zoom\Logger;

```

## Test

$ vendor/bin/phpunit
