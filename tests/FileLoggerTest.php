<?php

use org\bovigo\vfs\vfsStream;
use Zoom\Logger\FileLogger;

use Zoom\Utils\IP;


class FileLoggerTest extends \PHPUnit_Framework_TestCase
{
	private $root;
	private $configFile = 'logger.log';
	private $configPath = 'root';
	
	protected function setUp()
	{
		$this->root = vfsStream::setup($this->configPath);
	}

	/**
	 * @expectedException \Zoom\Exception\FileSystemException
	 */
	public function testValidateNotFoundFileInvalidArgumentException()
	{
		$root = vfsStream::setup($this->configPath, 200);
		$this->assertFalse($root->hasChild($this->configFile));
		$logger = new FileLogger('');
	}

	/**
	 * @expectedException \Zoom\Exception\NotWriteableException
	 */
	public function testValidateNotWriteableException()
	{
		$this->assertFalse($this->root->hasChild($this->configFile));
		vfsStream::newFile($this->configFile, 0400)->at($this->root)->setContent('');
		$obj = new FileLogger(vfsStream::url($this->configPath.'/'.$this->configFile));
	}

	public function testGetIp()
	{
		$this->assertFalse($this->root->hasChild($this->configFile));
		vfsStream::newFile($this->configFile, 0777)->at($this->root)->setContent('');

		$getIp = self::getMethod('getIp');
		$obj = new FileLogger(vfsStream::url($this->configPath.'/'.$this->configFile));

		$ip = $getIp->invokeArgs($obj, []);

		$this->assertRegExp('/\d+\.\d+\.\d+\.\d+/',$ip);
	}

	public function testGetTime()
	{
		$this->assertFalse($this->root->hasChild($this->configFile));
		vfsStream::newFile($this->configFile, 0777)->at($this->root)->setContent('');

		$getTime = self::getMethod('getTime');
		$obj = new FileLogger(vfsStream::url($this->configPath.'/'.$this->configFile));

		$date = $getTime->invokeArgs($obj, []);

		$this->assertRegExp('/\d{4}-\d{2}-\d{2} \d{2}:\d{1,2}:\d{1,2}/',$date);
	}

	public function testLog()
	{
		$this->assertFalse($this->root->hasChild($this->configFile));
		vfsStream::newFile($this->configFile, 0777)->at($this->root)->setContent('');

		$obj = new FileLogger(vfsStream::url($this->configPath.'/'.$this->configFile));
		$obj->log(\Psr\Log\LogLevel::DEBUG, 'this is a test');
		$this->assertRegexp('/this is a test/', $this->root->getChild($this->configFile)->getContent());
	}

	public function testDebug()
	{
		$this->assertFalse($this->root->hasChild($this->configFile));
		vfsStream::newFile($this->configFile, 0777)->at($this->root)->setContent('');

		$obj = new FileLogger(vfsStream::url($this->configPath.'/'.$this->configFile));
		$obj->debug('this is a test');
		$this->assertRegexp('/this is a test/', $this->root->getChild($this->configFile)->getContent());
	}

	protected static function getMethod($name)
	{
		$class = new ReflectionClass('\Zoom\Logger\FileLogger');
		$method = $class->getMethod($name);
		$method->setAccessible(true);
		return $method;
	}
}