<?php
namespace Zoom\Logger\Provider;

use Psr\Log\LogLevel;
use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;

use Zoom\Logger\AbstractLogger as ZoomAbstractLogger;
use Zoom\Logger\Logger;

/**
 * ZOOM Logger
 *
 * @package Zoom\Logger
 * @copyright Copyright (c) 2016 ZOOMcatalog
 * @author Enrique Ojeda <enriqueojedalara@gmail.com>
 */
final class SysLog extends ZoomAbstractLogger
{
	/**
	 * Logger status, if 0 will not log anything
	 * @var boolean
	 */
	protected $status = true;
	/**
	 * The string ident is added to each message.
	 * @var string
	 */
	public $ident = '';

	/**
	 * Log template
	 * @var string
	 */
	public $template = "{message} {context}";


	/**
	 * Constructor
	 * @param string $ident
	 */
	public function __construct(array $attributes = [])
	{
		if (!$this->status){
			return;
		}
		parent::__construct($attributes);

		if (empty($this->ident)) {
			$this->ident = 'zoomapi';
		}
		
		openlog($this->ident, LOG_PID | LOG_PERROR, LOG_LOCAL0);
	}

	/**
	 * Write in log
	 * @param mixed $level
	 * @param string $message
	 * @param array $context
	 * @return void
	 */
	public function log($level, $message, array $context = [])
	{
		$level = $this->mapLevel($level);
		if (null === $level) {
			return;
		}
		syslog($level, trim(strtr($this->template, [
			'{message}' => $message,
			'{context}' => $this->contextToString($context),
		])));
	}

	/**
	 * Match psr/log error levels to syslog error levels
	 * @param  $level
	 * @return 
	 */
	private function mapLevel($level)
	{
		$map = [
			LogLevel::EMERGENCY => LOG_EMERG,
			LogLevel::ALERT     => LOG_ALERT,
			LogLevel::CRITICAL  => LOG_CRIT,
			LogLevel::ERROR     => LOG_ERR,
			LogLevel::WARNING   => LOG_WARNING,
			LogLevel::NOTICE    => LOG_NOTICE,
			LogLevel::INFO      => LOG_INFO,
			LogLevel::DEBUG     => LOG_DEBUG,
		];
		return @$map[$level];
	}
}