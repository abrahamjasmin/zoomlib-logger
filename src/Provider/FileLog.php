<?php
namespace Zoom\Logger\Provider;

use Psr\Log\LogLevel;
use Psr\Log\AbstractLogger;
use Psr\Log\LoggerInterface;

use Zoom\Logger\Logger;
use Zoom\Logger\AbstractLogger as ZoomAbstractLogger;

use Zoom\Exception\NotWriteableException;
use Zoom\Exception\FileSystemException;
/**
 * ZOOM Logger
 *
 * @package Zoom\Logger
 * @copyright Copyright (c) 2016 ZOOMcatalog
 * @author Enrique Ojeda <enriqueojedalara@gmail.com>
 */
final class FileLog extends ZoomAbstractLogger
{
	/**
	 * Logger status, if 0 will not log anything
	 * @var boolean
	 */
	protected $status = true;

	/**
	 * Log file
	 * @var string
	 */
	public $logfile = '';

	/**
	 * Log template
	 * @var string
	 */
	public $template = "[{ip}|{datetime}] - {level}: {message} {context}";


	/**
	 * Constructor
	 * @param string $logfile
	 */
	public function __construct(array $attributes = [])
	{
		parent::__construct($attributes);
	}

	/**
	 * Write in log
	 * @param mixed $level
	 * @param string $message
	 * @param array $context
	 * @return void
	 */
	public function log($level, $message, array $context = [])
	{
		if (!$this->status){
			return;
		}

		file_put_contents($this->logfile, trim(strtr($this->template, [
			'{ip}'  => $this->getIp(),
			'{datetime}' => $this->getTime(),
			'{level}' => strtoupper($level),
			'{message}' => $message,
			'{context}' => $this->contextToString($context),
		])) . PHP_EOL, FILE_APPEND);
	}

	/**
	 * Validate file exists and it is writable
	 *
	 * @return void
	 * @throws Zoom\Exception\FileSystemException
	 * @throws Zoom\Exception\NotWriteableException
	 */
	private function validateFile()
	{
		if (!isset($this->logfile)) {
			$msg = sprintf('Internal server error, missing logger file');
			$detail = sprintf('FileLogger::logfile is not set');
			throw new FileSystemException($msg, 500, $detail);
		}

		if (!file_exists($this->logfile)) {
			if (!touch($this->logfile)) {
				$msg = sprintf('Internal server error, trying to create the log');
				$detail = sprintf('Log file %s cannot be created', $filename);
				throw new FileSystemException($msg, 500, $detail);
			}
		}

		if (!is_writable($this->logfile)) {
			$msg = sprintf('Internal server error, trying to write the log');
			$detail = sprintf('Log file %s is not writeable', $filename);
			throw new NotWriteableException($msg, 500, $detail);
		}
	}
}