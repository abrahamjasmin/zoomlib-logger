<?php
namespace Zoom\Logger;

use Zoom\Logger\AbstractLogger as ZoomAbstractLogger;

use Psr\Log\AbstractLogger as PsrAbstractLogger;
use Psr\Log\LoggerInterface;

/**
 * ZOOM Logger
 *
 * @package Zoom\Logger
 * @copyright Copyright (c) 2016 ZOOMcatalog
 * @author Enrique Ojeda <enriqueojedalara@gmail.com>
 */
final class Logger extends PsrAbstractLogger implements LoggerInterface
{
	/**
	 * Constructor
	 * @param string $logfile
	 */
	public function __construct(ZoomAbstractLogger $logger)
	{
		$this->logger = $logger;
	}

	/**
	 * Write in log
	 * @param mixed $level
	 * @param string $message
	 * @param array $context
	 * @return void
	 */
	public function log($level, $message, array $context = [])
	{
		$this->logger->log($level, $message, $context);
	}
}