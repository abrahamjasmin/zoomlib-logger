<?php
namespace Zoom\Logger;

use Psr\Log\LogLevel;
use Psr\Log\AbstractLogger as PsrAbstractLogger;
use Psr\Log\LoggerInterface;

use Zoom\DataTypes\IP;

/**
 * ZOOM abstract Logger
 *
 * @package Zoom\Logger
 * @copyright Copyright (c) 2016 ZOOMcatalog
 * @author Enrique Ojeda <enriqueojedalara@gmail.com>
 */
abstract class AbstractLogger extends PsrAbstractLogger implements LoggerInterface
{
	/**
	 * Constructor
	 *
	 * @param array $attributes
	 */
	public function __construct(array $attributes = [])
	{
		$reflection = new \ReflectionClass($this);
		foreach ($attributes as $attribute => $value)
		{
			$property = $reflection->getProperty($attribute);
			if ($property->isPublic())
			{
				$property->setValue($this, $value);
			}
		}
	}

	/**
	 * Update context to string
	 * 
	 * @param  array[Optional] $context
	 * @return string
	 */
	protected function contextToString(array $context = []) : string
	{
		return !empty($context) ? json_encode($context) : '';
	}

	/**
	 * Get current datetime
	 * @return string
	 */
	protected function getTime()
	{
		return date('Y-m-d H:i:s');
	}

	/**
	 * Get current IP
	 * @return string
	 */
	protected function getIp()
	{
		return (string)new IP();
	}
}